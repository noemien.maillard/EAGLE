#!/usr/bin/bash

intervals_files=/media/nomaillard/Storage/EAGLE/data/deepsea_genome_binned/deepsea_Mus_musculus_genome_binned/intervals.bins
deepsea=/home/nomaillard/Documents/Projects/EAGLE/pyScripts/05_deepsea_pred/01_run_deepsea_gpu.py
for i in {1..10}
do
	python3 deepsea --intervals-file $intervals_files$i.bed \
		--fasta-file /media/nomaillard/Storage/EAGLE/data/ref_seq/Mmusculus/Mmusculus_GRCm39/Mus_musculus.GRCm39.dna.toplevel_chrrenamed.fa \
		--names-file /home/nomaillard/Documents/deepsea/kipoi/predictor_names.txt \
		--keep-cols hesc \
		--output /media/nomaillard/Storage/EAGLE/results/deepsea/Mmusculus/mus_musculus_$i \
		--log /media/nomaillard/Storage/EAGLE/results/deepsea/Mmusculus/mus_musculus_$i.log
done
