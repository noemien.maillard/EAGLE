#!/usr/bin/bash

intervals_files=/media/nomaillard/Storage/EAGLE/data/deepsea_genome_binned/deepsea_Gallus_gallus_genome_binned/intervals.bins
deepsea=/home/nomaillard/Documents/Projects/EAGLE/pyScripts/05_deepsea_pred/01_run_deepsea_gpu.py
for i in {1..10}
do
	python3 $deepsea --intervals-file $intervals_files$i.bed \
		--fasta-file /media/nomaillard/Storage/EAGLE/data/ref_seq/Ggallus/Ggallus_bGalGal1_GRCg7b/Gallus_gallus.bGalGal1.mat.broiler.GRCg7b.dna.toplevel_chrrenamed.fa \
		--names-file /home/nomaillard/Documents/deepsea/kipoi/predictor_names.txt \
		--keep-cols ctcf h3k4me1 h3k4me3 h3k27ac h3k27me3 dnase \
		--drop-cols fetal smooth osteoblast fibroblast lymphoblast trophoblast derived \
		--output /media/nomaillard/Storage/EAGLE/results/deepsea/Ggallus/01_predictions/gallus_gallus_$i \
		--log /media/nomaillard/Storage/EAGLE/results/deepsea/Ggallus/01_predictions/gallus_gallus_$i.log
done
