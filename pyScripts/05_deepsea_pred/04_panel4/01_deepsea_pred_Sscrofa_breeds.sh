#!/usr/bin/bash

deepsea=/home/nomaillard/Documents/Projects/EAGLE/pyScripts/05_deepsea_pred/01_run_deepsea_gpu.py
breeds=(bamei berkshire hampshire jinhua landrace largewhite meishan pietrain rongchang tibetan usmarc wuzhishan)

declare -A dict_fasta
dict_fasta=(
	["bamei"]="Sus_scrofa_bamei.Bamei_pig_v1.dna.toplevel_chrrenamed.fa"
	["berkshire"]="Sus_scrofa_berkshire.Berkshire_pig_v1.dna.toplevel_chrrenamed.fa"
	["hampshire"]="Sus_scrofa_hampshire.Hampshire_pig_v1.dna.toplevel_chrrenamed.fa"
	["jinhua"]="Sus_scrofa_jinhua.Jinhua_pig_v1.dna.toplevel_chrrenamed.fa"
	["landrace"]="Sus_scrofa_landrace.Landrace_pig_v1.dna.toplevel_chrrenamed.fa"
	["largewhite"]="Sus_scrofa_largewhite.Large_White_v1.dna.toplevel_chrrenamed.fa"
	["meishan"]="Sus_scrofa_meishan.Meishan_pig_v1.dna.toplevel_chrrenamed.fa"
	["pietrain"]="Sus_scrofa_pietrain.Pietrain_pig_v1.dna.toplevel_chrrenamed.fa"
	["rongchang"]="Sus_scrofa_rongchang.Rongchang_pig_v1.dna.toplevel_chrrenamed.fa"
	["tibetan"]="Sus_scrofa_tibetan.Tibetan_Pig_v2.dna.toplevel_chrrenamed.fa"
	["usmarc"]="Sus_scrofa_usmarc.USMARCv1.0.dna.toplevel_chrrenamed.fa"
	["wuzhishan"]="Sus_scrofa_wuzhishan.minipig_v1.0.dna.toplevel_chrrenamed.fa"
)
for breed in ${breeds[@]}
do
	intervals_files="/media/nomaillard/Storage/EAGLE/data/deepsea_genome_binned/deepsea_Sus_scrofa_"$breed"_genome_binned/intervals.bins"
	output_dir="/media/nomaillard/Storage/EAGLE/results/deepsea/Sscrofa/Sscrofa_"$breed"/01_predictions/"
	mkdir -p $output_dir
	for i in {1..10}
	do
		python3 $deepsea --intervals-file $intervals_files$i.bed \
			--fasta-file /media/nomaillard/Storage/EAGLE/data/ref_seq/Sscrofa/Sscrofa_allbreeds_release112/${dict_fasta[$breed]} \
			--names-file /home/nomaillard/Documents/deepsea/kipoi/predictor_names.txt \
			--keep-cols ctcf h3k4me1 h3k4me3 h3k27ac h3k27me3 dnase \
			--drop-cols fetal smooth osteoblast fibroblast lymphoblast trophoblast derived \
			--output $output_dir"sus_scrofa_"$i \
			--log $output_dir"sus_scrofa_"$i.log
	done
done
