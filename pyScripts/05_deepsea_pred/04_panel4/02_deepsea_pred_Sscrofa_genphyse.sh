#!/usr/bin/bash

deepsea=/home/nomaillard/Documents/Projects/EAGLE/pyScripts/05_deepsea_pred/01_run_deepsea_gpu.py
genomes=(AB63083986_LW_ref AB75005564_LWm3 AB75005811_DU AB75069227_PE810 AB75069229_LRm6 AB75132735_LW AB75167259_PI AB75230671_LR AB75230687_TZM)

declare -A dict_fasta
dict_fasta=(
	["AB63083986_LW_ref"]="AB63083986_LW_ref.chrrenamed.fa"
	["AB75005564_LWm3"]="AB75005564_LWm3.chrrenamed.fa"
	["AB75005811_DU"]="AB75005811_DU.chrrenamed.fa"
	["AB75069227_PE810"]="AB75069227_PE810.chrrenamed.fa"
	["AB75069229_LRm6"]="AB75069229_LRm6.chrrenamed.fa"
	["AB75132735_LW"]="AB75132735_LW.chrrenamed.fa"
	["AB75167259_PI"]="AB75167259_PI.chrrenamed.fa"
	["AB75230671_LR"]="AB75230671_LR.chrrenamed.fa"
	["AB75230687_TZM"]="AB75230687_TZM.chrrenamed.fa"
)
for genome in ${genomes[@]}
do
	intervals_files="/media/nomaillard/Storage/EAGLE/data/deepsea_genome_binned/deepsea_Sus_scrofa_"$genome"_genome_binned/intervals.bins"
	output_dir="/media/nomaillard/Storage/EAGLE/results/deepsea/Sscrofa/Sscrofa_"$genome"/01_predictions/"
	mkdir -p $output_dir
	for i in {1..10}
	do
		python3 $deepsea --intervals-file $intervals_files$i.bed \
			--fasta-file /media/nomaillard/Storage/EAGLE/data/ref_seq/Sscrofa/Sscrofa_genphyse/${dict_fasta[$genome]} \
			--names-file /home/nomaillard/Documents/deepsea/kipoi/predictor_names.txt \
			--output $output_dir"sus_scrofa_"$i \
			--hdf-key intervals$i \
			--to-float16 \
			--log $output_dir"sus_scrofa_"$i.log
	done
done
