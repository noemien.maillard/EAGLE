#! /usr/bin/python3
# Creation date: 2024/10/22
# Last review: 2024/10/22
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: add coordinates to deepsea results.

import argparse
import logging
import natsort
import numpy as np
import pandas as pd
import re
import sys


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f',
        '--prediction-files',
        nargs='+',
      	help='List of prediction files.'
    )
    parser.add_argument(
        '-b',
        '--bed-files',
        nargs='+',
      	help='List of bed files.'
    )
    parser.add_argument(
        '--bed-sep',
        default='\t',
      	help='Separator for bed files. Default to tab.'
    )
    parser.add_argument(
        '-o',
        '--output-path',
        help='Output path. Input files will be save with the same name (but updated path)'
    )
    parser.add_argument(
        '--input-format',
        default='hdf',
        help='Input type format. Accepted values: csv, tsv, hdf and hdf5. Default to hdf. \
                Note that if you choose "csv" option, sep parameter will be the same for input and output.'
    )
    parser.add_argument(
        '--output-format',
        default='hdf',
        help='Output type format. Accepted values: csv, tsv, hdf and hdf5. Default to hdf. \
                Note that if you choose "csv" option, sep parameter will be the same for input and output.'
    )
    parser.add_argument(
        '--log',
        help='Log file name.'
    )
    return parser.parse_args()


def csv2csv(pred_list, bed_list, bed_sep, output_path):
    df_pred = pd.concat([pd.read_csv(file, sep=',') for file in pred_list]).reset_index(drop=True)
    df_bed = pd.concat([pd.read_csv(file, sep=bed_sep, names=['chr', 'start', 'end']) for file in bed_list]).reset_index(drop=True)
    df_bed = df_bed.sort_values(by='chr', key=lambda x: np.argsort(natsort.index_natsorted(df_bed['chr'])))
    df_pred = pd.concat([df_bed, df_pred], axis=1)
    for chr in df_pred['chr']:
        df = df_pred.loc[df_pred['chr']==chr,:]
        output_file = [x for x in pred_list if re.search(rf'\b{chr}\b', x)][0]
        output_file = output_file.split('/')[-1]
        df.to_csv('/'.join([output_path, output_file]), sep=',', index=False)


def csv2tsv(pred_list, bed_list, bed_sep, output_path):
    df_pred = pd.concat([pd.read_csv(file, sep=',') for file in pred_list]).reset_index(drop=True)
    df_bed = pd.concat([pd.read_csv(file, sep=bed_sep, names=['chr', 'start', 'end']) for file in bed_list]).reset_index(drop=True)
    df_bed = df_bed.sort_values(by='chr', key=lambda x: np.argsort(natsort.index_natsorted(df_bed['chr'])))
    df_pred = pd.concat([df_bed, df_pred], axis=1)
    for chr in df_pred['chr']:
        df = df_pred.loc[df_pred['chr']==chr,:]
        output_file = [x for x in pred_list if re.search(rf'\b{chr}\b', x)][0]
        output_file = re.sub('\\..*$', '.tsv', output_file.split('/')[-1])
        df.to_csv('/'.join([output_path, output_file]), sep='\t', index=False)


def csv2hdf(pred_list, bed_list, bed_sep, output_path):
    df_pred = pd.concat([pd.read_csv(file, sep=',') for file in pred_list]).reset_index(drop=True)
    df_bed = pd.concat([pd.read_csv(file, sep=bed_sep, names=['chr', 'start', 'end']) for file in bed_list]).reset_index(drop=True)
    df_bed = df_bed.sort_values(by='chr', key=lambda x: np.argsort(natsort.index_natsorted(df_bed['chr'])))
    df_pred = pd.concat([df_bed, df_pred], axis=1)
    for chr in df_pred['chr']:
        df = df_pred.loc[df_pred['chr']==chr,:]
        output_file = [x for x in pred_list if re.search(rf'\b{chr}\b', x)][0]
        output_file = re.sub('\\..*$', '.hdf5', output_file.split('/')[-1])
        df.to_hdf('/'.join([output_path, output_file]), key="df1", index=False)


def tsv2csv(pred_list, bed_list, bed_sep, output_path):
    df_pred = pd.concat([pd.read_csv(file, sep='\t') for file in pred_list]).reset_index(drop=True)
    df_bed = pd.concat([pd.read_csv(file, sep=bed_sep, names=['chr', 'start', 'end']) for file in bed_list]).reset_index(drop=True)
    df_bed = df_bed.sort_values(by='chr', key=lambda x: np.argsort(natsort.index_natsorted(df_bed['chr'])))
    df_pred = pd.concat([df_bed, df_pred], axis=1)
    for chr in df_pred['chr']:
        df = df_pred.loc[df_pred['chr']==chr,:]
        output_file = [x for x in pred_list if re.search(rf'\b{chr}\b', x)][0]
        output_file = re.sub('\\..*$', '.csv', output_file.split('/')[-1])
        df.to_csv('/'.join([output_path, output_file]), sep=",", index=False)


def tsv2tsv(pred_list, bed_list, bed_sep, output_path):
    df_pred = pd.concat([pd.read_csv(file, sep='\t') for file in pred_list]).reset_index(drop=True)
    df_bed = pd.concat([pd.read_csv(file, sep=bed_sep, names=['chr', 'start', 'end']) for file in bed_list]).reset_index(drop=True)
    df_bed = df_bed.sort_values(by='chr', key=lambda x: np.argsort(natsort.index_natsorted(df_bed['chr'])))
    df_pred = pd.concat([df_bed, df_pred], axis=1)
    for chr in df_pred['chr']:
        df = df_pred.loc[df_pred['chr']==chr,:]
        output_file = [x for x in pred_list if re.search(rf'\b{chr}\b', x)][0]
        output_file = output_file.split('/')[-1]
        df.to_csv('/'.join([output_path, output_file]), sep='\t', index=False)
            

def tsv2hdf(pred_list, bed_list, bed_sep, output_path):
    df_pred = pd.concat([pd.read_csv(file, sep='\t') for file in pred_list]).reset_index(drop=True)
    df_bed = pd.concat([pd.read_csv(file, sep=bed_sep, names=['chr', 'start', 'end']) for file in bed_list]).reset_index(drop=True)
    df_bed = df_bed.sort_values(by='chr', key=lambda x: np.argsort(natsort.index_natsorted(df_bed['chr'])))
    df_pred = pd.concat([df_bed, df_pred], axis=1)
    for chr in df_pred['chr']:
        df = df_pred.loc[df_pred['chr']==chr,:]
        output_file = [x for x in pred_list if re.search(rf'\b{chr}\b', x)][0]
        output_file = re.sub('\\..*$', '.hdf5', output_file.split('/')[-1])
        df.to_hdf('/'.join([output_path, output_file]), key="df1", index=False)


def hdf2csv(pred_list, bed_list, bed_sep, output_path):
    df_pred = pd.concat([pd.read_hdf(file, key='df1') for file in pred_list]).reset_index(drop=True)
    df_bed = pd.concat([pd.read_csv(file, sep=bed_sep, names=['chr', 'start', 'end']) for file in bed_list]).reset_index(drop=True)
    df_bed = df_bed.sort_values(by='chr', key=lambda x: np.argsort(natsort.index_natsorted(df_bed['chr'])))
    df_pred = pd.concat([df_bed, df_pred], axis=1)
    for chr in df_pred['chr']:
        df = df_pred.loc[df_pred['chr']==chr,:]
        output_file = [x for x in pred_list if re.search(rf'\b{chr}\b', x)][0]
        output_file = re.sub('\\..*$', '.csv', output_file.split('/')[-1])
        df.to_csv('/'.join([output_path, output_file]), sep=",", index=False)


def hdf2tsv(pred_list, bed_list, bed_sep, output_path):
    df_pred = pd.concat([pd.read_hdf(file, key='df1') for file in pred_list]).reset_index(drop=True)
    df_bed = pd.concat([pd.read_csv(file, sep=bed_sep, names=['chr', 'start', 'end']) for file in bed_list]).reset_index(drop=True)
    df_bed = df_bed.sort_values(by='chr', key=lambda x: np.argsort(natsort.index_natsorted(df_bed['chr'])))
    df_pred = pd.concat([df_bed, df_pred], axis=1)
    for chr in df_pred['chr']:
        df = df_pred.loc[df_pred['chr']==chr,:]
        output_file = [x for x in pred_list if re.search(rf'\b{chr}\b', x)][0]
        output_file = re.sub('\\..*$', '.tsv', output_file.split('/')[-1])
        df.to_csv('/'.join([output_path, output_file]), sep="\t", index=False)

            
def hdf2hdf(pred_list, bed_list, bed_sep, output_path):
    df_pred = pd.concat([pd.read_hdf(file, key='df1') for file in pred_list]).reset_index(drop=True)
    df_bed = pd.concat([pd.read_csv(file, sep=bed_sep, names=['chr', 'start', 'end']) for file in bed_list]).reset_index(drop=True)
    df_bed = df_bed.sort_values(by='chr', key=lambda x: np.argsort(natsort.index_natsorted(df_bed['chr'])))
    df_pred = pd.concat([df_bed, df_pred], axis=1)
    for chr in df_pred['chr']:
        df = df_pred.loc[df_pred['chr']==chr,:]
        output_file = [x for x in pred_list if re.search(rf'\b{chr}\b', x)][0]
        output_file = output_file.split('/')[-1]
        df.to_hdf('/'.join([output_path, output_file]), key='df1', index=False)


def main(pred_list, bed_list, bed_sep, output_path, input_format, output_format): 
    pred_list = natsort.natsorted(pred_list)
    bed_list = natsort.natsorted(bed_list)
    # input = csv
    if input_format == 'csv' and output_format == 'csv':
        csv2csv(pred_list, bed_list, bed_sep, output_path)
    elif input_format == 'csv' and output_format == 'tsv':
        csv2tsv(pred_list, bed_list, bed_sep, output_path)
    elif input_format == 'csv' and output_format in ['hdf', 'hfd5']:
        csv2hdf(pred_list, bed_list, bed_sep, output_path)

    # input = tsv
    elif input_format == 'tsv' and output_format == 'csv':
        csv2csv(pred_list, bed_list, bed_sep, output_path)
    elif input_format == 'tsv' and output_format == 'tsv':
        csv2tsv(pred_list, bed_list, bed_sep, output_path)
    elif input_format == 'tsv' and output_format in ['hdf', 'hfd5']:
        csv2hdf(pred_list, bed_list, bed_sep, output_path)

    # input = hdf5
    elif input_format in ['hdf', 'hdf5'] and output_format == 'csv':
        hdf2csv(pred_list, bed_list, bed_sep, output_path)
    elif input_format in ['hdf', 'hdf5'] and output_format == 'tsv':
        hdf2tsv(pred_list, bed_list, bed_sep, output_path)
    elif input_format in ['hdf', 'hdf5'] and output_format in ['hdf', 'hfd5']:
        hdf2hdf(pred_list, bed_list, bed_sep, output_path)


if __name__ == '__main__':
    args = get_options()

    logging.basicConfig(
	filename=args.log,
	filemode='w',
	level=logging.DEBUG,
	format='%(asctime)s %(message)s',
	datefmt='%Y/%d/%m %I:%M:%S %p'
    )

    logging.info(f'Input prediction files: {args.prediction_files}')
    logging.info(f'Input bed files: {args.bed_files}')
    logging.info(f'Input bed separator: {args.bed_sep}')
    logging.info(f'Output path: {args.output_path}')
    logging.info(f'Input format: {args.input_format}')
    logging.info(f'Output format: {args.output_format}')

    if args.input_format not in ['csv', 'tsv', 'hdf', 'hdf5']:
        raise ValueError('Accepted input formats: csv, tsv, hdf, hdf5')

    if args.output_format not in ['csv', 'tsv', 'hdf', 'hdf5']:
        raise ValueError('Accepted output formats: csv, tsv, hdf, hdf5.')

    main(
        pred_list=args.prediction_files,
        bed_list=args.bed_files,
        bed_sep=args.bed_sep,
	output_path=args.output_path,
        input_format=args.input_format,
        output_format=args.output_format
    )
