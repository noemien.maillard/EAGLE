# Creation date: 2024/07/25
# Last review: 2024/07/25
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Run DeepSEA (whole genome). Script adapted from jupyter notebook


import kipoi
import logging
from pandas import DataFrame, concat
from re import sub
from time import time


def get_options():
    import argparse


    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--intervals-file",
        help="Intervals file (i.e.kipoi)."
    )
    parser.add_argument(
        '--fasta-file',
        help="Fasta file (i.e. kipoi)."
    )
    parser.add_argument(
        '--names-file',
        help="File containing experiment names."
    )
    parser.add_argument(
        '--keep-cols',
        nargs='*',
        help="Columns to keep. First columns will be kept then dropped."
    )
    parser.add_argument(
        '--keep-cols-file',
        required=False,
        help="File containing 1 filter to keep per line."
    )
    parser.add_argument(
        '--drop-cols',
        nargs='*',
        help="Columns to drop. First columns will be kept then dropped."
    )
    parser.add_argument(
        '--drop-cols-file',
        required=False,
        help="File containing 1 filter to drop per line."
    )
    parser.add_argument(
        '-o',
        '--output',
        help="Output file name without extension (tsv)."
    )
    parser.add_argument(
        '--hdf-key',
        help="Key for hdf file save."
    )
    parser.add_argument(
        '--to-float16',
        action='store_true',
        help="If True, tables are saved in float16 (recommanded for big tables). Default to False (float32)."
    )
    parser.add_argument(
        "--log",
        required=False,
        help="Log file name. Default to '{args.output}.log'."
    )

    return parser.parse_args()


def run_deepsea(intervals_file, fasta_file):
    model = kipoi.get_model('DeepSEA/beluga')

    return model.pipeline.predict(dict(
        intervals_file=intervals_file,
        fasta_file=fasta_file
    ))


def keep_columns(df, cols):
    list_kept = []
    for col in cols:
        list_kept.append(df.loc[:,df.columns.str.contains(col, case=False)])
    
    return concat(list_kept, axis=1)


def drop_columns(df, cols):
    for col in cols:
        df = df.loc[:,~df.columns.str.contains(col, case=False)]
    
    return df


def main():
    args = get_options()
    
    if args.log is None:
        log = f'{args.output}.log'
    else:
        log = args.log
        
    logging.basicConfig(
            filename=log,
            filemode='w',
            level=logging.DEBUG,
            format='%(asctime)s %(message)s',
            datefmt='%Y/%d/%m %I:%M:%S %p'
            ) 
    
    logging.info(f'Intervals file: {args.intervals_file}')
    logging.info(f'Fasta file: {args.fasta_file}')
    logging.info(f'Output file: {args.output}.tsv')

    start = time()

    pred_names = open(args.names_file).read().splitlines()
    pred_names = [sub('_None$', '', name) for name in pred_names]
    results = DataFrame(run_deepsea(intervals_file=args.intervals_file, fasta_file=args.fasta_file), columns=pred_names)

    ## make column names unique
    # make a new data frame of column headers and number sequentially
    dfcolumns = DataFrame({'name': results.columns})
    dfcolumns['counter'] = dfcolumns.groupby('name').cumcount().apply(str)

    # remove counter for first case (optional) and combine suffixes
    dfcolumns.loc[dfcolumns.counter=='0', 'counter'] = ''
    results.columns = dfcolumns['name'] + dfcolumns['counter']

    if args.keep_cols_file is None and args.keep_cols is None:
        list_keep = []
    elif args.keep_cols_file is not None and args.keep_cols is None:
        list_keep = open(args.keep_cols_file).read().splitlines()
    elif args.keep_cols_file is None and args.keep_cols is not None:
        list_keep = args.keep_cols
    else:
        list_keep = open(args.keep_cols_file).read().splitlines()
        list_keep.append(args.keep_cols)
    logging.info(f'list of columns to keep: {[col for col in list_keep]}')

    if args.drop_cols_file is None and args.drop_cols is None:
        list_drop = []
    elif args.drop_cols_file is not None and args.drop_cols is None:
        list_drop = open(args.drop_cols_file).read().splitlines()
    elif args.drop_cols_file is None and args.drop_cols is not None:
        list_drop = args.drop_cols
    else:
        list_drop = open(args.drop_cols_file).read().splitlines()
        list_drop.append(args.drop_cols)
    logging.info(f'list of columns to drop: {[col for col in list_drop]}')


    if list_keep != []:
        results = keep_columns(df=results, cols=list_keep)
    if list_drop != []:
        results = drop_columns(df=results, cols=list_drop)

    if args.to_float16:
        results = results.astype('float16')

    if args.hdf_key:
        results.to_hdf(f'{args.output}.hdf5', key=args.hdf_key)
    else:
        results.to_csv(f'{args.output}.tsv', sep='\t', index=False)
    
    logging.info(f'Running time: {time()-start}')
    

if __name__ == "__main__":
    main()
