#! /usr/bin/python3
# Creation date: 2024/07/26
# Last review: 2024/07/26
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Concatenate deepSEA prediction files.

import argparse
from natsort import natsorted
import pandas as pd


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f',
        '--files',
        nargs='+',
        help='List of files to concatenate.'
    )
    parser.add_argument(
        '--keep',
        default=None,
        nargs='+',
        help='Columns to keep. List of filters (patterns). \
            Please note FIRST filters conditions to keep THEN drop conditions to drop.'
    )
    parser.add_argument(
        '--drop',
        default=None,
        nargs='+',
        help='Columns to drop. List of filters (patterns). \
            Please note FIRST filters conditions to keep THEN drop conditions to drop.'
    )
    parser.add_argument(
        '-o',
        '--output',
        help='Output file name.'
    )
    parser.add_argument(
        '--sep',
        default='\t',
        help='Input and output tables separator. Default to tab.'
    )
    return parser.parse_args()


def filter_table(df, filters_keep_list, filters_drop_list):
    list_filtered = []
    for condition in filters_keep_list:
        list_filtered.append(df.loc[:,df.columns.str.contains(condition, case=False)])
    
    kept_df = pd.concat(list_filtered, axis=1)
    for condition in filters_drop_list:
       kept_df = kept_df.loc[:,~kept_df.columns.str.contains(condition, case=False)]
    
    return kept_df


def sort_and_concat(files_list, sep, keep_list, drop_list):
    # sort files in numerical order
    sorted_list = natsorted(files_list)
    # read files in list ready to concatenate
    df_list = []
    for file in sorted_list:
        df = pd.read_csv(file, sep=sep, engine="python")
        df = filter_table(df, filters_keep_list=keep_list, filters_drop_list=drop_list)
        df_list.append(df)

    return pd.concat(df_list)


if __name__ == '__main__':
    args = get_options()
    sort_and_concat(
        files_list=args.files,
        sep=args.sep,
        keep_list=args.keep,
        drop_list=args.drop
    ).to_csv(args.output, sep=args.sep, index=False)
