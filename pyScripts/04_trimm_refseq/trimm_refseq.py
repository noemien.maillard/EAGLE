#! /usr/bin/python3
# Creation date: 2024/07/12
# Last review: 2024/07/12
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Trimm reference sequences from N at start and end of chromosomes.


from Bio import SeqIO
from Bio.Seq import Seq
import gzip
import logging
import multiprocessing as mp
import os
import psutil
from re import sub
from time import time


def get_options():
    import argparse


    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--paths',
        nargs="*",
        help="List of path containing files. All files in each path will be considered as fasta.gz."
    )
    parser.add_argument(
        '--files',
        nargs="*",
        help="List of files in fasta.gz format."
    )
    parser.add_argument(
        '--ext',
        default='.fa.gz',
        help="Files extension to consider. Default to '.fa.gz'."
    )
    parser.add_argument(
        '-p',
        '--processes',
        type=int,
        default=1,
        help='Number of worker processes to use. Default to 1.'
    )
    parser.add_argument(
        '--log',
        help="Log file name."
    )

    return parser.parse_args()
    


def trimm_sequence(seq):
    new_seq = sub('^N+', '', seq)
    del seq

    return sub('N+$', '', new_seq)


def trimm_and_write(input_file, output_file):
    base_mem = psutil.virtual_memory().active
    start = time()
    with gzip.open(input_file, "rt") as in_file:
        with gzip.open(output_file, "wt") as out_file:
            for record in SeqIO.parse(in_file, "fasta"):
                logging.info(f"Chromosome name: {record.id}")
                trimmed_seq = Seq(trimm_sequence(str(record.seq)))
                record.seq = trimmed_seq
                used_mem = (psutil.virtual_memory().active-base_mem)/1_000_000_000
                logging.info(f"Memory use: {round(used_mem, ndigits=1)}G")
                SeqIO.write(record, handle=out_file, format="fasta")
    
    logging.info(f"Input: {input_file}\nOutput: {output_file}")
    logging.info(f"Running time (sec): {time()-start}")


def main(list_files, processes):
    runs_args = [[file, sub(".fa.gz", "_trimmed.fa.gz", file)] for file in list_files]
    with mp.Pool(processes=processes) as pool:
        pool.starmap(trimm_and_write, runs_args)


if __name__ == '__main__':
    args = get_options()

    # concatenate files from files and paths arguments
    list_files = []
    if args.paths:
        for path in args.paths:
            for root, _, files in os.walk(path):
                for file in files:
                    if file.endswith(args.ext):
                        list_files.append(os.path.join(root, file))
    if args.files:
        list_files.extend(args.files)

    logging.basicConfig(
        filename=args.log,
        filemode='w',
        level=logging.DEBUG,
        format='%(asctime)s %(message)s',
        datefmt='%Y/%d/%m %I:%M:%S %p',
        encoding='utf-8'
        ) 

    main(list_files, processes=args.processes)
