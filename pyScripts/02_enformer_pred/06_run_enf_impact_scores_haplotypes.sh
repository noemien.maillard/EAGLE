#!/usr/bin/bash

# blood
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/reconstructed_haplotype/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/blood.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/blood/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/blood/blood_haplo.log
# brain
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/reconstructed_haplotype/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/brain.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/brain/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/brain/brain_haplo.log
# fat
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/reconstructed_haplotype/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/fat.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/fat/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/fat/fat_haplo.log
# glands
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/reconstructed_haplotype/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/glands.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/glands/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/glands/glands_haplo.log
# muscle
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/reconstructed_haplotype/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/muscle.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/muscle/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/muscle/muscle_haplo.log
# pancreas
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/reconstructed_haplotype/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/pancreas.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/pancreas/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/pancreas/pancreas_haplo.log
# reproduction
python3 01_run_enformer.py --fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/reconstructed_haplotype/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/repro.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/repro/ \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/repro/repro_haplo.log

# uncomment to shut down pc when run is done
#poweroff
