#!/usr/bin/bash

source ~/miniconda3/etc/profile.d/conda.sh

conda activate enformerenv

genomes=(AB63083986_LW_ref AB75005564_LWm3 AB75005811_DU AB75069227_PE810 AB75069229_LRm6 AB75132735_LW AB75167259_PI AB75230671_LR AB75230687_TZM)

for genome in ${genomes[@]}
do
	# make predictions
	results_dir="/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa_"$genome"/01_predictions"
	mkdir -p $results_dir
	python3 01_run_enformer.py --fasta-path "/media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_"$genome"_genome_binned/" \
		--results-path $results_dir/all_expe/ \
		--to-hdf \
		--to-float16 \
		--log $results_dir/all_expe.log

	# fat filters (liver and adipose)
		#--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/BSgenome/filter_files/fat.txt \
		#--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	# muscle filters
		#--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/BSgenome/filter_files/muscle.txt \
		#--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	# lung filters
		#--filters-keep lung \
		#--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
	# spleen filters
		#--filters-keep spleen \
		#--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin derived \
done
