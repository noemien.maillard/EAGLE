#!/usr/bin/bash
#SBATCH -J enformer_addcoords
#SBATCH -o /home/nmaillard/work/enformer/Sscrofa/add_coords.out
#SBATCH -e /home/nmaillard/work/enformer/Sscrofa/add_coords.err
#SBATCH --mem=10G
#SBATCH --mail-user=noemien.maillard@inrae.fr
#SBATCH --mail-type=BEGIN,END,FAIL


module purge
module load devel/python/Python-3.9.18


filtered_df_path=/home/nmaillard/work/enformer/Sscrofa/
bed_path=/work/genphyse/genepi/noemien/bed/
#ori_df_path=/work/genphyse/genepi/noemien/01_predictions/deepsea/Sscrofa/
breeds=(Sscrofa_AB63083986_LW_ref Sscrofa_AB75005564_LWm3 Sscrofa_AB75005811_DU Sscrofa_AB75069227_PE810 Sscrofa_AB75069229_LRm6 Sscrofa_AB75132735_LW Sscrofa_AB75167259_PI Sscrofa_AB75230671_LR Sscrofa_AB75230687_TZM)
tissues=(adipose liver lung muscle spleen)

for breed in ${breeds[@]}
do
	for tissue in ${tissues[@]}
	do
		#mkdir --parents $out_path$breed
		python3 add_coords.py --prediction-files `find $filtered_df_path$breed/$tissue -name *hdf5` \
			--bed-files `find $bed_path$breed -name *bed` \
			--output-path $filtered_df_path$breed/$tissue \
			--input-format hdf5 \
			--output-format tsv \
			--log $filtered_df_path$breed/$tissue/add_coords.log
	done
done
