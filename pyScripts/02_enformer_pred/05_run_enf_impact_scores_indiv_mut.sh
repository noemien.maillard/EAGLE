#!/usr/bin/bash

ref=(LWsnp LWins LWdels)
alt=(MSsnp MSins MSdels)
name=(20240516_ImpactScores_SNP 20240516_ImpactScores_INS 20240516_ImpactScores_DEL)
for i in {0..2}
do
# blood
python3 01_run_enformer.py --score-variants \
	--ref ${ref[i]} \
	--alt ${alt[i]} \
	--name-output-scores ${name[i]} \
	--fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/individual_mutations/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/blood.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/blood/individual_mutations/  \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/blood/blood.log
# brain
python3 01_run_enformer.py --score-variants \
	--ref ${ref[i]} \
	--alt ${alt[i]} \
	--name-output-scores ${name[i]} \
	--fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/individual_mutations/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/brain.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/brain/individual_mutations/  \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/brain/brain.log
# fat
python3 01_run_enformer.py --score-variants \
	--ref ${ref[i]} \
	--alt ${alt[i]} \
	--name-output-scores ${name[i]} \
	--fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/individual_mutations/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/fat.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/fat/individual_mutations/  \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/fat/fat.log
# glands
python3 01_run_enformer.py --score-variants \
	--ref ${ref[i]} \
	--alt ${alt[i]} \
	--name-output-scores ${name[i]} \
	--fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/individual_mutations/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/glands.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/glands/individual_mutations/  \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/glands/glands.log
# muscle
python3 01_run_enformer.py --score-variants \
	--ref ${ref[i]} \
	--alt ${alt[i]} \
	--name-output-scores ${name[i]} \
	--fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/individual_mutations/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/muscle.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/muscle/individual_mutations/  \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/muscle/muscle.log
# pancreas
python3 01_run_enformer.py --score-variants \
	--ref ${ref[i]} \
	--alt ${alt[i]} \
	--name-output-scores ${name[i]} \
	--fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/individual_mutations/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/pancreas.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/pancreas/individual_mutations/  \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/pancreas/pancreas.log
# reproduction
python3 01_run_enformer.py --score-variants \
	--ref ${ref[i]} \
	--alt ${alt[i]} \
	--name-output-scores ${name[i]} \
	--fasta-path /media/nomaillard/Storage/EAGLE/data/enformer_impact_scores/individual_mutations/ \
	--filters-keep /media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/filter_files/repro.txt \
	--filters-drop fetal embryo crispr fibrobl endothelial cell_line origin \
	--results-path /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/repro/individual_mutations/  \
	--log /media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/repro/repro.log
done

# uncomment to shut down pc when run is done
#poweroff
