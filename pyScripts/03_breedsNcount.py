from Bio import SeqIO
import gzip
import json
import re


def get_options():
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--input-files',
	nargs='+',
        help='List of input files'
    )

    return parser.parse_args()


def main():
    args = get_options()
    
    for file in args.input_files:
        dict_breed = dict()
        with gzip.open(file, 'rt') as fa:
            for seq in SeqIO.parse(fa, "fasta"):
                dict_breed[seq.id] = re.findall('N[N]+', str(seq.seq))
        with open(str(file).replace('.fa.gz', 'Ncount.json'), 'w') as outfile:
            json.dump(dict_breed, outfile, indent=2)


if __name__ == '__main__':
    main()

