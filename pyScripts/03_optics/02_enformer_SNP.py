from matplotlib.gridspec import GridSpec
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.cluster import OPTICS
from sklearn.preprocessing import scale


# center and reduce
# absolute values -> manhattan metric
# suggestion: ball-tree algo
# notes related to k-nearest algorithms: with many outliers, use high value of k.
    # odd values of k are recommended to avoid ties in classification (cross-validation).

# load tables
dataSNP = pd.read_csv(
    "/media/nomaillard/Storage/EAGLE/data/deepbind_impact_scores/20231005_snp.csv",
    sep=";",
)

imp_scor_SNP = pd.read_csv(
    "/media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/fat/individual_mutations/20240516_ImpactScores_SNP.csv",
    sep=",",
)

# basic preprocessing of tables (renaming, sorting, insert/droping)
dataSNP.rename(columns={'POS': 'snp'}, inplace=True)
dataSNP.sort_values(by="snp", inplace=True)
imp_scor_SNP.insert(loc=0, column='snp', value=dataSNP.loc[:,"snp"])
imp_scor_SNP.sort_values(by="snp", inplace=True)


# create column with alternative allele (from LW or MS) to put in IS table
dataSNP["allele"]="MS"
dataSNP.loc[dataSNP["LW"]==1, "allele"]="LW"

imp_scor_SNP.insert(loc=1, column='alt', value=dataSNP['allele'])

# drop SNP showing IS==0
imp_scor_SNP = imp_scor_SNP.loc[~(imp_scor_SNP.iloc[:,2:]==0).all(axis=1)]
imp_scor_SNP.reset_index(drop=True, inplace=True)

# scale IS
imp_scor_SNP = pd.concat([
    imp_scor_SNP.iloc[:,:2],
    pd.DataFrame(
        scale(imp_scor_SNP.iloc[:,2:], with_mean=True, with_std=True),
        columns=imp_scor_SNP.columns[2:]
        )  # closes pd.DataFrame
    ], axis=1)  # closes pd.concat

optics = OPTICS(
    metric='manhattan',
    xi=0.01,
    min_cluster_size=0.95,
    algorithm='ball_tree',
)

optics.fit(imp_scor_SNP.iloc[:,2:])

space = np.arange(len(imp_scor_SNP))
reachability = optics.reachability_[optics.ordering_]
labels = optics.labels_[optics.ordering_]

imp_scor_SNP.insert(loc=2, column='labels', value=optics.labels_)

plt.figure(figsize=(10, 7))
G = GridSpec(2, 3)
ax1 = plt.subplot(G[0, :])
ax2 = plt.subplot(G[1, 0])
ax3 = plt.subplot(G[1, 1])
ax4 = plt.subplot(G[1, 2])

# Reachability plot
colors = list(np.random.choice(range(256), size=len(set(labels))))
for klass, color in zip(range(0, len(colors)), colors):
    Xk = space[labels == klass]
    Rk = reachability[labels == klass]
    ax1.plot(Xk, Rk, color, alpha=0.3)
ax1.plot(space[labels == -1], reachability[labels == -1], "k.", alpha=0.3)
# ax1.plot(space, np.full_like(space, 2.0, dtype=float), "k-", alpha=0.5)
# ax1.plot(space, np.full_like(space, 0.5, dtype=float), "k-.", alpha=0.5)
ax1.set_ylabel("Reachability (epsilon distance)")
ax1.set_title("Reachability Plot")

min_x = min(imp_scor_SNP.iloc[:,3])-1
max_x = max(imp_scor_SNP.iloc[:,3])+1
min_y = min(imp_scor_SNP.iloc[:,4])-1
max_y = max(imp_scor_SNP.iloc[:,4])+1
xlab = imp_scor_SNP.columns.tolist()[3]
ylab = imp_scor_SNP.columns.tolist()[4]

# OPTICS
for klass, color in zip(range(0, 5), colors):
    Xk = imp_scor_SNP.loc[imp_scor_SNP['labels'] == klass,:].iloc[:,[3,4]].values.tolist()
    x = [coord[0] for coord in Xk]
    y = [coord[1] for coord in Xk]
    ax2.scatter(x, y, s=1, c=[color]*len(x), alpha=0.3)
outclust = imp_scor_SNP.loc[imp_scor_SNP['labels'] == -1,:].iloc[:,[3,4]].values.tolist()
x_outclust = [coord[0] for coord in outclust]
y_outclust = [coord[1] for coord in outclust]
ax2.plot(x_outclust, y_outclust, "k+", alpha=0.1)
ax2.set_ylabel(ylab)
ax2.set_xlim([min_x, max_x])
ax2.set_ylim([min_y, max_y])
ax2.set_title("Automatic Clustering\nOPTICS")

for klass, color in zip(range(0, 5), colors):
    Xk = imp_scor_SNP.loc[imp_scor_SNP['labels'] == klass,:].iloc[:,[3,4]].values.tolist()
    x = [coord[0] for coord in Xk]
    y = [coord[1] for coord in Xk]
    ax3.scatter(x, y, s=1, c=[color]*len(x), alpha=0.3)
ax3.set_xlabel(xlab)
ax3.set_xlim([min_x, max_x])
ax3.set_ylim([min_y, max_y])
ax3.set_title("Dropped ouliers\nOPTICS")

ax4.plot(x_outclust, y_outclust, "k+")
ax4.set_xlim([min_x, max_x])
ax4.set_ylim([min_y, max_y])
ax4.set_title("Outliers\nOPTICS")

plt.tight_layout()
# print(imp_scor_SNP.loc[imp_scor_SNP["labels"]==-1])
# plt.show()
plt.savefig(
    fname="/media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/20240524_plot_impact_scores/20240524_individual_vars/03_optics/optics_95.png",
    format='png'
)

imp_scor_SNP.loc[imp_scor_SNP["labels"]==-1].to_csv(
    "/media/nomaillard/Storage/EAGLE/results/enformer/240307_impact_scores_Lw_MS/20240524_plot_impact_scores/20240524_individual_vars/03_optics/optics_95.tsv",
    sep="\t",
    index=False
)
