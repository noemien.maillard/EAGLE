import  natsort
import os
import pandas as pd


def get_options():
    import argparse

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--input-files',
        nargs='+',
        help='List of input files.'
    )
    parser.add_argument(
        '--input-format',
        help='Format of input files. Accepted values: csv, hdf/hdf5.'
    )
    parser.add_argument(
        '--output-format',
        help='Format of output files. Accepted values: csv, hdf/hdf5.'
    )
    parser.add_argument(
        '--sep',
        default='\t',
        help='Separator for csv formatted files. Default to tab.'
    )
    parser.add_argument(
        '--rm-input',
        action='store_true',
        help='If true, remove input files after formatting. Default to false.'
    )

    return parser.parse_args()


def csv2hdf(input_files, sep):
    output_files = [file[:-3]+'hdf5' for file in input_files]
    for input, output in zip(input_files, output_files):
        df = pd.read_csv(input, sep=sep)
        df.to_hdf(output, key="df1")


def hdf2csv(input_files, sep):
    if sep == "\t":
        output_files = [file[:-4]+'tsv' for file in input_files]
    elif sep == ",":
        output_files = [file[:-4]+'csv' for file in input_files]

    for input, output in zip(input_files, output_files):
        df = pd.read_hdf(input, key="df1")
        df.to_csv(output, sep=sep)


def main():
    args = get_options()

    if args.sep not in ['\t', ',']:
        raise ValueError('Separator not accepted. Separator must be tab or comma.')
    
    if args.input_format == 'csv' and args.output_format in ['hdf', 'hfd5']:
        csv2hdf(
            input_files=natsort.natsorted(args.input_files),
            sep=args.sep
        )
    elif args.input_format in ['hdf', 'hdf5'] and args.output_format == 'csv':
        hdf2csv(
            input_files=natsort.natsorted(args.input_files),
            sep=args.sep
        )
    else:
        raise ValueError('Incompatibility of input/output formats. Values unknown or identical.')
    
    if args.rm_input:
        for file in args.input_files:
            os.remove(file)


if __name__ == '__main__':
    main()
