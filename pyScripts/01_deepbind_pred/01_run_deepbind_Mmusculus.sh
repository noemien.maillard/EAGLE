#!/usr/bin/bash


# load deepbind parameters
deepbind='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/deepbind'
params='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/db/samples/Mmusculus'
mmusculus='/media/nomaillard/Storage/EAGLE/data/deepbind_genome_binned/deepbind_Mus_musculus_genome_binned/'

for scaffold in `ls $mmusculus`
do
	echo "calculating scaffold $scaffold..."
	# load binned fasta
	list_binned_fasta=`ls $mmusculus$scaffold/*.fa`
	# store outputs with same name as binned fasta but with txt extension
	outputs=$(for file in $list_binned_fasta; do echo ${file%.*}".txt";done)

	# run deepbind
	python deepbind_useCPU.py --deepbind $deepbind --params $params --binned-fasta $list_binned_fasta --outputs $outputs -p 20 --log $mmusculus$scaffold/$scaffold.log
done

