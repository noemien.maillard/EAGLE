#!/usr/bin/bash


## ChIP hepG2
deepbind='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/deepbind'
params='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/db/samples/ChIP_hepG2'
fasta_dir='/media/nomaillard/Storage/EAGLE/data/deepbind_impact_scores/reconstructed_haplotype/'

echo "calculating with params in $params..."
# load binned fasta
list_binned_fasta=`ls $fasta_dir*.fa`
# store outputs with same name as binned fasta but with txt extension
outputs=$(for file in $list_binned_fasta; do echo ${file%.*}".txt";done)

# run deepbind
python deepbind_useCPU.py --deepbind $deepbind --params $params --binned-fasta $list_binned_fasta --outputs $outputs -p 5

for file in $fasta_dir*.txt
do 
	filename=`basename $file`
	mv $file '/media/nomaillard/Storage/EAGLE/results/deepbind/240306_impact_scores_Lw_MS/'${filename%.*}"_ChIP_hepG2.txt"
done


## ChIP CTCF
params='/home/nomaillard/Documents/Collab_GenEpi/programs/deepbind/db/samples/ctcf'

echo "calculating with params in $params..."
# load binned fasta
list_binned_fasta=`ls $fasta_dir*.fa`
# store outputs with same name as binned fasta but with txt extension
outputs=$(for file in $list_binned_fasta; do echo ${file%.*}".txt";done)

# run deepbind
python deepbind_useCPU.py --deepbind $deepbind --params $params --binned-fasta $list_binned_fasta --outputs $outputs -p 5

for file in $fasta_dir*.txt
do 
	filename=`basename $file`
	mv $file '/media/nomaillard/Storage/EAGLE/results/deepbind/240306_impact_scores_Lw_MS/'${filename%.*}"_ctcf.txt"
done
