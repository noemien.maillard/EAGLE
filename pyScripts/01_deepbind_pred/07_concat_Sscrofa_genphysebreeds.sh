#!/usr/bin/bash


concat_df=/home/nomaillard/Documents/Projects/EAGLE/pyScripts/02_concat_df.py

breeds=(Sscrofa_AB63083986_LW_ref Sscrofa_AB75005564_LWm3 Sscrofa_AB75005811_DU Sscrofa_AB75069227_PE810 Sscrofa_AB75069229_LRm6 Sscrofa_AB75132735_LW Sscrofa_AB75167259_PI Sscrofa_AB75230671_LR Sscrofa_AB75230687_TZM)

# load directory containing 1 directory per scaffold and output
chr_dirs='/media/nomaillard/Storage/EAGLE/results/deepbind/Sscrofa'		
output_path='/media/nomaillard/Storage/EAGLE/results/deepbind/Sscrofa'

for breed in ${breeds[@]}
do
	echo $breed
	breed_chr_dirs=$chr_dirs/$breed/01_predictions
	breed_output=$output_path/$breed/01_predictions/whole_genome/
	mkdir -p $breed_output

	for scaffold in `ls $breed_chr_dirs`;
	do
		if [[ $scaffold == 'whole_genome' ]]; then continue; fi
		echo "concatenating files for $scaffold..."
		# scores_files are files containing predictions
		scores_files=`ls $chr_dirs/$breed/01_predictions/$scaffold/*.txt`
		python $concat_df --files $scores_files --output $breed_output$scaffold.bin.deepbind.txt
	done
done
