#!/usr/bin/bash

# ## ChIP hepG2
# # load directory containing 1 directory per scaffold and output
# chr_dirs='/home/nomaillard/Documents/Collab_GenEpi/data/Sscrofa_genome_binned/ChIP_hepG2/'
# output_path='/home/nomaillard/Documents/predictions_from_Sscrofa_refseq/deepbind/whole_genome/ChIP_hepG2/'

# ## ChIP CTCF
# # load directory containing 1 directory per scaffold and output
# chr_dirs='/home/nomaillard/Documents/Collab_GenEpi/data/Sscrofa_genome_binned/ctcf/'
# output_path='/home/nomaillard/Documents/predictions_from_Sscrofa_refseq/deepbind/whole_genome/ctcf/'

# ## ChIP Mmusculus
# # load directory containing 1 directory per scaffold and output
# chr_dirs='/media/nomaillard/Storage/EAGLE/results/deepbind/Mmusculus/01_predictions/'
# output_path='/media/nomaillard/Storage/EAGLE/results/deepbind/Mmusculus/01_predictions/whole_genome/'

## ChIP Hsapiens
# load directory containing 1 directory per scaffold and output
chr_dirs='/media/nomaillard/Storage/EAGLE/results/deepbind/Hsapiens/01_predictions/'
output_path='/media/nomaillard/Storage/EAGLE/results/deepbind/Hsapiens/01_predictions/whole_genome/'

mkdir -p $output_path

for scaffold in `ls $chr_dirs`;
do
	if [[ $scaffold == 'whole_genome' ]]; then continue; fi
	echo "concatenating files for $scaffold..."
	# scores_files are files containing predictions
	scores_files=`ls $chr_dirs$scaffold/*.txt`
	python concat_deepbind.py --files $scores_files --output $output_path$scaffold.bin.deepbind.txt
done
