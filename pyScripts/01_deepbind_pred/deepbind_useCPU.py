#! /usr/bin/python3
# Creation date: 2023/11/17
# Last review: 2023/12/12
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Parallelize deepbind predictions.


import argparse
import multiprocessing as mp
import logging
import subprocess as sp
import sys
from time import time


def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--deepbind',
        help='Deepbind executable file (including path).'
    )
    parser.add_argument(
        '--params',
        help='Deepbind parameters path. The last directory should contain all params files.'
    )
    parser.add_argument(
        '-f',
        '--binned-fasta',
        nargs='+',
        help='List of fasta files containing binned sequences.'
    )
    parser.add_argument(
        '-o',
        '--outputs',
        nargs='+',
        help='List of output files name including path.'
    )
    parser.add_argument(
        '-p',
        '--processes',
        type=int,
        default=None,
        help='Number of worker processes to use. Default to None so the number returned by os.cpu_count() is used.'
    )
    parser.add_argument(
        '--log',
        help="Log file name."
    )
    return parser.parse_args()


def sp_deepbind(db_path, ids_path, data_path, output_path):
    logging.info(f"{db_path} `ls {ids_path} | sed s/.txt//g` < {data_path} > {output_path}")

    # run sh command
    sp.Popen(
        f"{db_path} `ls {ids_path} | sed s/.txt//g` < {data_path} > {output_path}",
        shell=True
    ).wait()


if __name__ == '__main__':
    args = get_options()

    logging.basicConfig(
        filename=args.log,
        filemode='w',
        level=logging.DEBUG,
        format='%(asctime)s %(message)s',
        datefmt='%Y/%d/%m %I:%M:%S %p',
        encoding='utf-8'
        ) 

    # check as much outputs than inputs
    if len(args.binned_fasta) != len(args.outputs):
        logging.error('Binned fasta and outputs have to be of same length. Exit code 1')
        sys.exit('Binned fasta and outputs have to be of same length. Exit code 1')

    # order fasta files and output files
    list_binned_fasta = list(sorted(args.binned_fasta))
    list_outputs = list(sorted(args.outputs))
    # create a list of arguments for each run
    runs_args = [[args.deepbind, args.params, fasta, output] for fasta, output in zip(list_binned_fasta, list_outputs)]
    
    start = time()
    # parallelize runs
    with mp.Pool(processes=args.processes) as pool:
        pool.starmap(sp_deepbind, runs_args)
    
    logging.info(f"Running time (sec): {time()-start}")
