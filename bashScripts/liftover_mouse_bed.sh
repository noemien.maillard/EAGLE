#!/usr/bin/bash


tf_down="/media/nomaillard/Storage/EAGLE/data/Mmusculus/bed/01_downloaded/remap_ft"
tf_lifted="/media/nomaillard/Storage/EAGLE/data/Mmusculus/bed/02_liftover/remap_ft"
hist_down="/media/nomaillard/Storage/EAGLE/data/Mmusculus/bed/01_downloaded/encode_histones"
hist_lifted="/media/nomaillard/Storage/EAGLE/data/Mmusculus/bed/02_liftover/encode_histones"
chain="/media/nomaillard/Storage/EAGLE/data/chain_over/mm10ToMm39.over.chain.gz"
liftOver="/home/nomaillard/Programs/liftOver"
tmp_dir="/media/nomaillard/Storage/EAGLE/data/Mmusculus/bed/tmp/"

mkdir $tmp_dir

# transcription factors
for bed in `ls $tf_down/*.bed.gz`
do
	cut_bed=$(basename $(echo $bed | sed 's/.bed.gz/_cut.bed/'))
	zcat $bed | cut -f 1-3 > $tmp_dir$cut_bed
	new_bed=$(basename $(echo $bed | sed 's/.bed.gz/_lifted.bed/'))
	unmapped=$(basename $(echo $bed | sed 's/.bed.gz/_unMapped.bed/'))
	$liftOver $tmp_dir$cut_bed $chain $tf_lifted/$new_bed $tf_lifted/$unmapped
done

# histone marks
for bed in `ls $hist_down/*.bed.gz`
do
	cut_bed=$(basename $(echo $bed | sed 's/.bed.gz/_cut.bed/'))
	zcat $bed | cut -f 1-3 > $tmp_dir$cut_bed
	new_bed=$(basename $(echo $bed | sed 's/.bed.gz/_lifted.bed/'))
	unmapped=$(basename $(echo $bed | sed 's/.bed.gz/_unMapped.bed/'))
	$liftOver $tmp_dir$cut_bed $chain $hist_lifted/$new_bed $hist_lifted/$unmapped
done

rm -r $tmp_dir
