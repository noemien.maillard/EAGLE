#!/usr/bin/env Rscript

# Creation date: 2024/07/19
# Last review: 2024/07/19
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Calculate auROC, auPR for DeepSEA predictions on whole genome (WG). Adapted from 01_auROC_auPRC_parser.R (deepsea)
# Modules versions: R version 4.4.0, data.table 1.15.0, dplyr 1.1.4, GenomicRanges 1.56.0, pracma 2.4.4, PRROC 1.3.1, rtracklayer 1.62.0


library(dplyr)
library(GenomicRanges)
library(logr)
library(PRROC)


get_options <- function(){
  library(argparse)
  
  
  parser <- ArgumentParser()
  
  parser$add_argument(
    "--pred_path",
    help="Path where bigwig files (DeepSEA predictions) are stored."
  )
  parser$add_argument(
    "--protein",
    help="Protein to grep in observation and prediction paths (case insensitive)."
  )
  parser$add_argument(
    "--tissues",
    nargs="*",
    help="Tissues to grep in observation and prediction paths (case insensitive)."
  )
  parser$add_argument(
    "--obs_path",
    help="Path containing observed data. All '.bed' files will be considered.\n
      Observed data must not have header and must contain 3 columns (chrom, start, end) (use of rtracklayer)."
  )
  parser$add_argument(
    "--fread_obs",
    action="store_true",
    help="If True, use fread from data.table and converts to GRanges. Default to False (use of rtracklayer)."
  )
  parser$add_argument(
    "--obs_files",
    nargs="*",
    help="Observed data '.bed' files.\n
      Observed data must not have header and must contain 3 columns (chrom, start, end)."
  )
  parser$add_argument(
    "-o",
    "--output",
    help="Output name of the table (tsv format, no extension added)."
  )
  parser$add_argument(
    "--chr_pattern",
    help="Pattern to remove from chromosome names. Recommended: '_dna.*'."
  )
  parser$add_argument(
    "--add_chr",
    action="store_true",
    help="If TRUE, adds 'chr' at start of each chromosome. Default to FALSE."
  )
  parser$add_argument(
    "--lifted",
    action="store_true",
    help="Find 'lifted' pattern in obs files. Useful when lifterover have been used. Default to False."
  )
  parser$add_argument(
    "--log",
    help="Log file (name). Default to $save_path/deepsea_auROC_auPRC.log"
  )
  
  return(parser$parse_args())
}


# # test args
# args <- list(
#   pred_path="/media/nomaillard/Storage/EAGLE/results/deepsea/Ggallus/02_bigwig/",
#   protein="ctcf",
#   tissues=list("hepg2", "liver"),
#   obs_path="/media/nomaillard/Storage/EAGLE/data/Ggallus/bed_ncbi_geo",
#   obs_files=NULL,
#   fread_obs=T,
#   output="/media/nomaillard/Storage/EAGLE/results/deepsea/auc_deepsea/Ggallus/test.tsv",
#   chr_pattern=NULL,
#   add_chr=F,
#   lifted=F,
#   log="/media/nomaillard/Storage/EAGLE/results/deepsea/auc_deepsea/Ggallus/test.log"
# )

args <- get_options()

log <- log_open(
  args$log,
  logdir=F
)

log_code()
log_print(paste0("Predictions path: ", args$pred_path))
log_print(paste0("Protein: ", args$protein))
log_print(paste0("Tissues: ", args$tissues))
log_print(paste0("Observations path: ", args$obs_path))
log_print(paste0("Observation files: ", args$obs_files))
log_print(paste0("fread_obs: ", args$fread_obs))
log_print(paste0("Output: ", args$output))
log_print(paste0("Chromosomes pattern to remove: ", args$chr_pattern))
log_print(paste0("add_chr: ", args$add_chr))
log_print(paste0("lifted: ", args$lifted))
log_print(paste0("Log file: ", args$log))


# load deepsea bigwig and keep scores > 0 (peak probability of 50%)
pred_path <- sub('/$', '', args$pred_path)
deepsea.list_bw <- list.files(pred_path, full.names=T, pattern=args$protein, ignore.case=T)
deepsea.list_bw <- deepsea.list_bw[grep('\\.bw', deepsea.list_bw)]
if(!is.null(args$tissues)){
  deepsea.list_bw <- unlist(lapply(args$tissues, grep, x=deepsea.list_bw, ignore.case=T, value=T))
}
deepsea.list <- lapply(deepsea.list_bw, rtracklayer::import.bw)
if(! is.null(args$chr_pattern) & args$add_chr){
  deepsea.list <- lapply(
    seq_along(deepsea.list),
    function(i){
      gr <- deepsea.list[[i]]
      seq.names <- paste0('chr', sub(args$chr_pattern, '', seqnames(gr)))
      GRanges(
        seq.names,
        IRanges(start(gr), end(gr)),
        score=gr$score
      )
    })
}else if(! is.null(args$chr_pattern) & ! args$add_chr){
  deepsea.list <- lapply(
    seq_along(deepsea.list),
    function(i){
      gr <- deepsea.list[[i]]
      seq.names <- sub(args$chr_pattern, '', seqnames(gr))
      GRanges(
        seq.names,
        IRanges(start(gr), end(gr)),
        score=gr$score
      )
    })
}else if(is.null(args$chr_pattern) & args$add_chr){
  deepsea.list <- lapply(
    seq_along(deepsea.list),
    function(i){
      gr <- deepsea.list[[i]]
      seq.names <- paste0('chr', seqnames(gr))
      GRanges(
        seq.names,
        IRanges(start(gr), end(gr)),
        score=gr$score
      )
    })
}

# load observations (merging of bed files)
# !!! obs.list is a vector !!!
if(!is.null(args$obs_path) & is.null(args$obs_files)){
  obs.list <- list.files(sub('/$', '', args$obs_path), full.names=T, pattern=args$protein, ignore.case=T, recursive=T)
  obs.list <- obs.list[grep('\\.bed', obs.list)]
}else if(!is.null(args$obs_path) & !is.null(args$obs_files)){
  obs.list <- list.files(sub('/$', '', args$obs_path), full.names=T, pattern=args$protein, ignore.case=T, recursive=T)
  obs.list <- obs.list[grep('\\.bed', obs.list)]
  obs.list <- c(obs.list, args$obs_files)
  obs.list <- obs.list[grep(args$protein, obs.list, ignore.case=T)]
}else if(is.null(args$obs_path) & !is.null(args$obs_files)){
  obs.list <- args$obs_files
}else{
  # kill run if no path and files argument
  obs.list <- NULL
}

if (args$lifted){
  obs.list <- obs.list[grep('lifted', obs.list, ignore.case=T)]
}

if(!is.null(args$tissues)){
  obs.list <- unlist(lapply(args$tissues, grep, x=obs.list, ignore.case=T, value=T))
}

if(args$fread_obs){
  observations <- lapply(
    obs.list,
    function(x){
      bed <- data.table::fread(x, select=c(1:3), col.names=c('chr', 'start', 'end'))
      GRanges(
        seqnames=bed$chr,
        IRanges(start=bed$start, end=bed$end)
      )
    }
  )
}else{
  observations <- lapply(obs.list, rtracklayer::import.bed)
}


df.auc <- data.table::rbindlist(unlist(lapply(
  seq_along(deepsea.list),
  function(i){
    deepsea <- deepsea.list[[i]]
    # for "prediction" column
    pred.file <- unlist(strsplit(deepsea.list_bw[i], "/"))
    pred.file <- pred.file[length(pred.file)]
    
    lapply(
      seq_along(observations),
      function(j){
        obs <- observations[[j]]
        common_seqnames <- intersect(as.character(unique(obs@seqnames)), as.character(unique(deepsea@seqnames)))
        pred <- keepSeqlevels(deepsea, common_seqnames, pruning.mode="coarse")
        obs <- keepSeqlevels(obs, common_seqnames, pruning.mode="coarse")

        # extract peaks in deepsea ranges (for each chromosome) (mainly a checkpoint due to common_seqnames step)
        obs <- unlist(GRangesList(lapply(
          unique(obs@seqnames),
          function(chr){
            chr.gr <- keepSeqlevels(pred, as.character(chr), pruning.mode="coarse")
            obs.chr <- obs[seqnames(obs)==chr &
                             start(obs)>start(chr.gr)[1]-1 &
                             end(obs)<tail(end(chr.gr), n=1)+1]
            obs.chr
          }
        )))
        
        log_print(paste0("common_seqnames: ", common_seqnames))
        
        pred$label <- 0
        pred[queryHits(findOverlaps(pred, obs))]$label <- 1
        
        
        # Calculate deepsea auROC and auPR for CTCF and save df
        obs.file <- unlist(strsplit(obs.list[j], "/"))
        obs.file <- obs.file[length(obs.file)]
        list(
          pred.file,
          obs.file,
          length(obs),
          args$protein,
          args$tissues,
          roc.curve(scores.class0=pred$score[pred$label==1], scores.class1=pred$score[pred$label==0], curve=F)$auc,
          pr.curve(scores.class0=pred$score[pred$label==1], scores.class1=pred$score[pred$label==0], curve=F)$auc.integral
        )
      }
    )  # close 2nd lapply
  }
), recursive=F))  # close 1st lapply, unlist and rbindlist

colnames(df.auc) <- c("prediction", "observation", "observed peaks", "protein", "tissue", "auROC", "auPR")

write.table(
  df.auc,
  file=args$output,
  sep='\t',
  col.names=F,
  row.names=F,
  quote=F
)

log_print(paste0("Script run: DONE."))

log_close()
