#!/usr/bin/bash


deepsea_path=/media/nomaillard/Storage/EAGLE/results/deepsea/Sscrofa/
breeds=(Sscrofa_AB63083986_LW_ref Sscrofa_AB75005564_LWm3 Sscrofa_AB75005811_DU Sscrofa_AB75069227_PE810 Sscrofa_AB75069229_LRm6 Sscrofa_AB75132735_LW Sscrofa_AB75167259_PI Sscrofa_AB75230671_LR Sscrofa_AB75230687_TZM)

for breed in ${breeds[@]}
do
	mkdir --parents $deepsea_path$breed'/02_biwig/'
	./03_bed2bigwig_nobed_parser.R --results $deepsea_path$breed'/01_predictions/concat_chroms.tsv' \
		--save_path $deepsea_path$breed'/02_biwig/'
done
