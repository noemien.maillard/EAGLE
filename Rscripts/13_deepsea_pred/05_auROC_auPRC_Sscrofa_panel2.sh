#!/usr/bin/bash


# species dna
pred_path=/media/nomaillard/Storage/EAGLE/results/deepsea/Sscrofa/Sscrofa11.1/02_bigwig
output_path=/media/nomaillard/Storage/EAGLE/results/deepsea/auc_deepsea/02_panel2/Sscrofa/Sscrofa11.1

# proteins and observations
proteins=(CTCF H3K4me1 H3K4me3 H3K27ac H3K27me3)
Sscrofa_obs_path=/media/nomaillard/Storage/EAGLE/data/Sscrofa/bed_ncbi_geo

declare -A t_dict
t_dict=(["liver"]="liver hepg2" ["lung"]="lung a549" ["muscle"]="muscle hsmm")

for protein in ${proteins[@]}
do
	for tissue in ${!t_dict[@]}
	do
		./04_auROC_auPRC_parser.R \
			--pred_path $pred_path \
			--protein $protein \
			--tissues ${t_dict[$tissue]} \
			--obs_path $Sscrofa_obs_path \
			--fread_obs \
			--output $output_path/$protein$tissue"_Sscrofa.tsv" \
			--log $output_path/$protein$tissue"_Sscrofa.log"
	done
done
