#!/usr/bin/bash


# species dna
pred_path=/media/nomaillard/Storage/EAGLE/results/deepsea/Ggallus/02_bigwig
output_path=/media/nomaillard/Storage/EAGLE/results/deepsea/auc_deepsea/02_panel2/Ggallus

# proteins and observations
proteins=(CTCF H3K4me1 H3K4me3 H3K27ac H3K27me3)
Ggallus_obs_path=/media/nomaillard/Storage/EAGLE/data/Ggallus/bed_ncbi_geo

declare -A t_dict
t_dict=(["liver"]="liver hepg2" ["lung"]="lung a549" ["muscle"]="muscle hsmm")

for protein in ${proteins[@]}
do
	for tissue in ${!t_dict[@]}
	do
		./04_auROC_auPRC_parser.R \
			--pred_path $pred_path \
			--protein $protein \
			--tissues ${t_dict[$tissue]} \
			--obs_path $Ggallus_obs_path \
			--fread_obs \
			--output $output_path/$protein$tissue"_Ggallus.tsv" \
			--log $output_path/$protein$tissue"_Ggallus.log"
	done
done
