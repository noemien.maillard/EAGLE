ref_seq=/media/nomaillard/Storage/EAGLE/data/ref_seq/Sscrofa/Sscrofa_genphyse/
for genome in `ls $ref_seq*chrrenamed.fa`;
do
	org_name=$(echo $genome | grep -Eo './\w+\.')
	org_name=${org_name:2:-1}
	org_dir="/media/nomaillard/Storage/EAGLE/data/enformer_genome_binned/enformer_Sus_scrofa_"$org_name"_genome_binned/"
	mkdir -p $org_dir
	
	./07_bin_fasta.R -g $genome --bed_path $org_dir --log $org_dir$org_name.log
done
