#!/usr/bin/bash


# species dna
pred_path=/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa11.1/02_bigwig
output_path=/media/nomaillard/Storage/EAGLE/results/enformer/auc_enformer/01_panel2/Sscrofa/Sscrofa11.1

# proteins and observations
proteins=(CTCF H3K4me1 H3K4me3 H3K27ac H3K27me3)
Sscrofa_obs_path=/media/nomaillard/Storage/EAGLE/data/Sscrofa/bed_ncbi_geo

declare -A t_dict
t_dict=(["adipose"]="adipose fat" ["liver"]="liver hepg2" ["lung"]="lung" ["muscle"]="muscle myo" ["spleen"]="spleen")

for protein in ${proteins[@]}
do
	for tissue in ${!t_dict[@]}
	do
		if [[ $tissue = "adipose" ]] || [[ $tissue = "liver" ]]
		then
			./09_auROC_auPRC_parser.R \
				--pred_path $pred_path/fat/ \
				--protein $protein \
				--tissues ${t_dict[$tissue]} \
				--obs_path $Sscrofa_obs_path \
				--fread_obs \
				--output $output_path/$protein$tissue"_Sscrofa.tsv" \
				--log $output_path/$protein$tissue"_Sscrofa.log"
		else
			continue
			./09_auROC_auPRC_parser.R \
				--pred_path $pred_path/$tissue/ \
				--protein $protein \
				--tissues ${t_dict[$tissue]} \
				--obs_path $Sscrofa_obs_path \
				--fread_obs \
				--output $output_path/$protein$tissue"_Sscrofa.tsv" \
				--log $output_path/$protein$tissue"_Sscrofa.log"
		fi
	done
done
