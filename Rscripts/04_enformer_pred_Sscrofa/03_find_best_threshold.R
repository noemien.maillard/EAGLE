#!/usr/bin/env Rscript
# Creation date: 2024/02/06
# Last review: 2024/02/06
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Determine best threshold for enformer predictions. Inspired from 04_reduce_peaks_and_find_best_threshold_ChIPhepG2.R (EAGLE/Rscripts/deepbind)
# Comments: Count enough RAM (>15G). Running time ~7-8h (~840 expe).
# WARNING: Because of purrr (parallel) use, the script cannot be used on windows.
# Modules versions: R version 4.3.2, GenomicRanges 1.54.1, ggplot2 3.4.4, pracma 2.4.4, reshape 1.4.4, rtracklayer 1.62.0, tools 4.3.2


library(ggplot2)


setwd("/media/nomaillard/Storage/EAGLE/plots_best_thresholds/enformer/")

# init functions to calculate best threshold
calc_thresholds <- function(seq.min, seq.max, length.out, gr_pred){
  # calc peaks for each threshold
  test.thresholds <- seq(seq.min, seq.max, length.out=length.out)
  nbins <- vapply(
    test.thresholds,
    function(t){
      return (length(gr_pred[gr_pred$score>t]))
    },
    FUN.VALUE=numeric(1)
  )
  npeaks <- vapply(
    test.thresholds,
    function(t){
      smpl.gr_pred <- gr_pred[gr_pred$score>t]
      smpl_enf.pred.reduced <- GenomicRanges::reduce(smpl.gr_pred, with.revmap = TRUE)
      
      return (length(smpl_enf.pred.reduced))
    },
    FUN.VALUE=numeric(1)
  )
  # extract best threshold, plot data and save
  table <- data.frame(test.thresholds, nbins, npeaks)
  colnames(table) <- c("thresholds", "n.bins", "n.peaks")

  return (table)
}

calc_best_threshold <- function(expe, npeaks_h, npeaks_expected, gr_pred, plot.name){
  # step1 - calculate thresholds along whole ranges
  peaks_cnt1 <- calc_thresholds(seq.min=0, seq.max=max(gr_pred$score), length.out=100, gr_pred=gr_pred)
  # step2 - setup min and max to calculate thresholds in reduced range of interest
  # Note: sometimes the peak is thin so min.t is -Inf
  min.t <- max(peaks_cnt1$thresholds[peaks_cnt1$n.peaks >= npeaks_expected])
  if (is.infinite(min.t)){
    nmax_idx <- which(peaks_cnt1$n.peaks==max(peaks_cnt1$n.peaks))
    list_rows <- as.vector(lapply(nmax_idx, function(x)(x-1):(x+1))[[1]])
    min.t <- peaks_cnt1$thresholds[unlist(list_rows[1])]
    max.t <- peaks_cnt1$thresholds[unlist(list_rows[3])]
  }else{
    max.t <- min(peaks_cnt1$thresholds[peaks_cnt1$threshold > min.t])
  }
  peaks_cnt2 <- calc_thresholds(seq.min=min.t, seq.max=max.t, length.out=10, gr_pred=gr_pred)
  # step3
  min.t <- max(peaks_cnt2$thresholds[peaks_cnt2$n.peaks >= npeaks_expected])
  if (is.infinite(min.t)){
    nmax_idx <- which(peaks_cnt1$n.peaks==max(peaks_cnt1$n.peaks))
    list_rows <- as.vector(lapply(nmax_idx, function(x)(x-1):(x+1))[[1]])
    min.t <- peaks_cnt1$thresholds[unlist(list_rows[1])]
    max.t <- peaks_cnt1$thresholds[unlist(list_rows[3])]
  }else{
    max.t <- min(peaks_cnt1$thresholds[peaks_cnt1$threshold > min.t])
  }
  peaks_cnt3 <- calc_thresholds(seq.min=min.t, seq.max=max.t, length.out=10, gr_pred=gr_pred)
  
  # concat df and free memory
  peaks_cnt <- rbind(peaks_cnt1, peaks_cnt2, peaks_cnt3)
  remove(peaks_cnt1)
  remove(peaks_cnt2)
  remove(peaks_cnt3)
  peaks_cnt <- peaks_cnt[!duplicated(peaks_cnt$thresholds),]
  peaks_cnt <- peaks_cnt[order(peaks_cnt$thresholds),]
  
  # extract curve peak with thresholds of interest
  peaks_cnt_sub <- peaks_cnt[peaks_cnt$n.peaks >= npeaks_expected,]
  
  # best threshold
  best_t <- c(
    expe,
    npeaks_h,
    npeaks_expected,
    peaks_cnt_sub$n.peaks[peaks_cnt_sub$thresholds==max(peaks_cnt_sub$thresholds)],
    peaks_cnt_sub$thresholds[peaks_cnt_sub$thresholds==max(peaks_cnt_sub$thresholds)]
    )
  
  best_t_plot <- ggplot(peaks_cnt) +
    geom_line(aes(x=thresholds, y=n.peaks), color='green3') +
    geom_line(aes(x=thresholds, y=n.bins), color='blue4') +
    geom_hline(yintercept=npeaks_expected, color='red') +
    geom_text(data=subset(peaks_cnt, thresholds == best_t[5]), aes(x=thresholds, y=n.peaks, label=round(thresholds, digits=6), vjust=-1.5, hjust=-0.25)) +
    scale_y_log10(breaks = scales::trans_breaks("log10", function(x) 10^x), labels = scales::trans_format("log10", scales::math_format(.x))) +
    theme_classic(base_size=18) +
    theme(legend.position="none", plot.title=element_text(hjust=0.5)) +
    labs(title=expe) +
    ylab("log n peaks")
  ggsave(
    filename=plot.name,
    plot=best_t_plot
  )
  
  return(best_t)
}

# genomes lengths
### WARNING: Enformer does not predict on whole genome so we have to consider
  # pig genome size is the predicted range. For this, we take an example of the
  # list of predictions (the range is the same for all examples)
list_files <- list.files("/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/bigwig/blood/")
list_files <- list_files[which(lapply(
  list_files,
  function(x){grep("CAGE.bone_marrow._adult", x)}
)!=0)]
list_files <- paste0("/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/bigwig/blood/", list_files)
gr_mimic_genome <- lapply(list_files, rtracklayer::import.bw)
sscrofa_genome_len <- sum(vapply(
  gr_mimic_genome,
  function(chrom){
    return(max(GenomicRanges::end(chrom))-min(GenomicRanges::start(chrom)))
  },
  FUN.VALUE=numeric(1)
))
remove(list_files)
remove(gr_mimic_genome)
hsapiens_genome_len <- sum(GenomeInfoDb::seqlengths(BSgenome.Hsapiens.UCSC.hg38::BSgenome.Hsapiens.UCSC.hg38))

# path and load for human bed files
save_path <- 'results_bed_from_ENCODE/'
path_h <- 'bed_ENCODE/bed_files/'
files_h <- list.files(path_h)
ATAC_h <- paste0(path_h, "ATAC.bed")
CAGE_h <- paste0(path_h, "CAGE.bed")
DNASE_h <- paste0(path_h, "DNASE.bed")

# store protein names, function from tools lib
prot_names <- tools::file_path_sans_ext(files_h)

tissues <- c("fat", "muscle", "blood", "brain", "glands", "pancreas", "repro")
for (tissue in tissues){
  print(tissue)
  save_path <- paste0("/media/nomaillard/Storage/EAGLE/plots_best_thresholds/enformer/results_bed_from_ENCODE/", tissue, "/")
  path_tiss <- paste0("/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa_genome_binned/bigwig/", tissue, "/")

  # get enformer files for the given tissue
  files_enf <- tools::file_path_sans_ext(
    list.files(path_tiss)
    )  # close file_path_sans_ext
  # substitute prefix to work by condition (and merge all chr for each)
  files_enf <- gsub(".*393216b_", "", files_enf)
  
  # init list to store output (for fat (most expe), n=328)
  list_thresholds <- vector("list", length(unique(files_enf)))
  i=1
  for (expe in unique(files_enf)){
    print(expe)
    # obs = GRanges(human observed) (according to experimental condition)
    if(startsWith(expe, "CAGE")){
      obs <- rtracklayer::import.bed(CAGE_h)
    }else if(startsWith(expe, "CHIP")){
      # grep CHIPed protein
      prot <- prot_names[which(lapply(
        prot_names,
        function(x){grep(paste0("\\b", x, "\\b"), expe)}
      )!=0)]  # "\\b" to take the exact prot name (to avoid Pol2Aphospho for example)
      # grep human CHIP
      obs <- paste0(path_h, files_h[which(lapply(
        files_h,
        function(x){grep(paste0("\\b", prot, "\\b"), x)}
      )!=0)])
      obs <- rtracklayer::import.bed(obs)
    }else if(startsWith(expe, "ATAC")){
      # if expe has human tissue spe reference
      if (1 %in% lapply(c("fat", "muscle", "pancreas"), grep, expe)){
        atac <- files_h[which(lapply(
          files_h,
          function(x){grep("ATAC", x)}
        )!=0)]
        obs <- paste0(path_h, atac[which(lapply(
          atac,
          function(x){grep(tissue, x)}
        )!=0)])
        obs <- rtracklayer::import.bed(obs)
      }else{
        obs <- rtracklayer::import.bed(ATAC_h)
      }
    }else if(startsWith(expe, "DNASE")){
      # if expe has human tissue spe reference
      if (1 %in% lapply(c("fat", "muscle", "pancreas"), grep, expe)){
        dnase <- files_h[which(lapply(
          files_h,
          function(x){grep("DNASE", x)}
        )!=0)]
        obs <- paste0(path_h, dnase[which(lapply(
          dnase,
          function(x){grep(tissue, x)}
        )!=0)])
        obs <- rtracklayer::import.bed(obs)
      }else{
        obs <- rtracklayer::import.bed(DNASE_h)
      }
    }  # end if/else expe cond
    
    # load predictions
    pred <- list.files(path_tiss)
    pred <- pred[which(lapply(
      pred,
      function(x){grep(expe, x)}
    )!=0)]
    list_pred <- paste0(path_tiss, pred)
    enf.pred <- unlist(GenomicRanges::GRangesList(lapply(list_pred, rtracklayer::import.bw)))
    # calc best_threshold and append list
    list_thresholds[[i]] <- calc_best_threshold(
      expe=expe,
      npeaks_h=length(obs),
      npeaks_expected=round(length(obs)*sscrofa_genome_len/hsapiens_genome_len),
      gr_pred=enf.pred,
      plot.name=paste0(save_path, "plots_peak_threshold/", expe, ".png")
      )
    i=i+1
  }
  table <- data.table::rbindlist(lapply(list_thresholds, as.list))
  colnames(table) <- c("expe.name", "human.cnt", "expected.cnt", "n.peaks", "threshold")
  write.table(table, paste0(save_path, "recap_", tissue, "_best_thresholds.tsv"), quote=F, sep='\t', row.names=F)
}
