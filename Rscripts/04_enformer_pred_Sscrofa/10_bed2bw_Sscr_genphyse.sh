#!/usr/bin/bash


enf_pred=/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/
breeds=(Sscrofa_AB63083986_LW_ref Sscrofa_AB75005564_LWm3 Sscrofa_AB75005811_DU Sscrofa_AB75069227_PE810 Sscrofa_AB75069229_LRm6 Sscrofa_AB75132735_LW Sscrofa_AB75167259_PI Sscrofa_AB75230671_LR Sscrofa_AB75230687_TZM)
tissues=(adipose liver lung muscle spleen)

for breed in ${breeds[@]}
do
	pred_breed=$enf_pred$breed
	for tissue in ${tissues[@]}
	do
		pred_breed_tissue=$pred_breed'/01_predictions/'$tissue
		bw_breed_tissue=$pred_breed'/02_bigwig/'$tissue

		mkdir --parents $bw_breed_tissue

		./02_bed2bigwig_nobed_parser.R --results_path $pred_breed_tissue \
			--results_sep '\t' \
			--results_extension '*.tsv' \
			--save_path $bw_breed_tissue
	done
done
