#!/usr/bin/env Rscript


# Creation date: 2024/08/21
# Last review: 2024/08/21
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Generate bw files from enformer prediction data. Inspired from 02_bed2bigwig_PorQTL (EAGLE).
# Modules versions: R version 4.4.1, argparse_2.2.3, data.table 1.15.0, GenomicRanges 1.54.1, logr 1.3.8, rtracklayer 1.62.0


library(GenomicRanges)
library(logr)


get_options <- function(){
  library(argparse)
  
  
  parser <- ArgumentParser()
  parser$add_argument(
    '--results_path',
    help='Path containing results files (recommended to run by tissue).'
  )
  parser$add_argument(
    '--results_sep',
    default=',',
    help='Separator of input results files. Default to comma.'
  )
  parser$add_argument(
    '--results_extension',
    default='*.csv',
    help='Extension pattern to look for. Default to "*.csv".'
  )
  parser$add_argument(
    "--save_path",
    help="Path where bigwig files have to be saved."
  )
  parser$add_argument(
    '--log',
    default=NULL,
    help='Log file name. Default to "$save_path/bed2bw.log".'
  )
  
  return(parser$parse_args())
}

# args test
args <- list(
  'results_path'='/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa_AB63083986_LW_ref/01_predictions/adipose/',
  'results_sep'='\t',
  'results_extension'='*.tsv',
  'save_path'='/media/nomaillard/Storage/EAGLE/results/enformer/Sscrofa/Sscrofa_AB63083986_LW_ref/02_bigwig/adipose/'
)

args <- get_options()

# to be sure that save_path end with 1 "/"
print(args$save_path)
save_path <- sub('/$', '', args$save_path)
save_path <- paste0(save_path, '/')

if (!dir.exists(save_path)){
  dir.create(save_path, recursive=T)
}

if (is.null(args$log)){
  log <- log_open(
    paste0(save_path, 'bed2bw.log'),
    logdir=F
  )
}else{
  log <- log_open(
    args$log,
    logdir=F
  )
}

log_print(paste0("results_path: ", args$results_path))
log_print(paste0("results_sep: ", args$results_sep))
log_print(paste0("results_extension: ", args$results_extension))
log_print(paste0("save_path: ", args$save_path))

log_code()

results_path <- sub('/$', '', args$results_path)
# get file names without extension
file.names <- tools::file_path_sans_ext(
  list.files(
    results_path,
    pattern=args$results_extension,
    recursive=T
  )  # close list.files
)  # close file_path_sans_ext

enf.files <- list.files(results_path, pattern=args$results_extension, full.names=T, recursive=T)

for (chrom in file.names){
  log_print(paste0('chromosome: ', chrom))

  # select and read enformer results
  enf.file <- enf.files[which(lapply(
    file.names,
    function(x){grep(paste0('\\b', chrom, '\\b'), x)}
  )!=0)]
  enf.df <- data.table::fread(
    enf.file,
    header=T,
    sep=args$results_sep
  )
  
  # split df into bed and experiments
  bed.df <- enf.df[, c('chr', 'start', 'end')]
  enf.df <- enf.df[, -c('chr', 'start', 'end')]
  # remove points at the end of names
  names(enf.df) <- sub('\\.$', '', names(enf.df))
  
  grnames <- Rle(bed.df$chr)
  grpos <- IRanges(start=bed.df$start, end=bed.df$end)
  lapply(
    colnames(enf.df),
    function(expe){
      # as.vector(unlist()) to extract only values
      scores <- as.vector(unlist(enf.df[,..expe]))
      gr <- GRanges(
        seqnames=grnames,
        ranges=grpos,
        score=scores
      )
      
      # mandatory line for exporting
      seqlengths(gr) <- max(split(end(gr), seqnames(gr)))
      filename <- paste0(save_path, chrom, '_', gsub(':', '_', expe), '.bw')
      rtracklayer::export.bw(gr, filename)
    }
  )
}

log_close()
