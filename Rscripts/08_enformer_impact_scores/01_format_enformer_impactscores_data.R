# Creation date: 2024/03/07
# Last review: 2024/03/07
# By Noémien MAILLARD
# Laboratory: GENEPI
# Project: EAGLE
# Script aim: Prepare fasta and bed files to calculate variant impact
# IMPORTANT POINT: ref=large white (LW) and alt=meishan (MS)
# Modules versions: R version 4.3.3, ### CHECK PACKAGES AND VERSIONS ### Biostrings 2.70.2, BSgenome 1.70.2, BSgenome.Sscrofa.UCSC.susScr11 1.4.2, GenomicRanges 1.54.1, IRanges 2.36.0


setwd("/media/nomaillard/Storage/EAGLE/")


dataSNP <- read.table("data/deepbind_impact_scores/20231005_snp.csv", sep=';', header=T)

dataSNP$CHR <- "chr1"
dataSNP <- dataSNP[c(length(dataSNP), c(1:length(dataSNP)-1))]

# replace 0s and 1s to REF and ALT content
dataSNP$LW <- unlist(lapply(
  seq_along(dataSNP$LW),
  function(x){
    if(dataSNP$LW[x]==0){
      dataSNP$REF[x]
    }else{
      dataSNP$ALT[x]
    }
  }
))
dataSNP$MS <- unlist(lapply(
  seq_along(dataSNP$MS),
  function(x){
    if(dataSNP$MS[x]==0){
      dataSNP$REF[x]
    }else{
      dataSNP$ALT[x]
    }
  }
))

# # drop REF and ALT columns
# dataSNP <- dataSNP[-c(3, 4)]

receptive_field=393216  # receptive field of enformer
window=114688  # predicted window of enformer is 114688
bin.size=128

# setup GRanges with LW and MS alleles and get sequences
bed.GR=GenomicRanges::GRanges(
  dataSNP[,1],
  IRanges::IRanges(dataSNP[,2], dataSNP[,2]),
  LW=dataSNP[,3],
  MS=dataSNP[,4]
)
region.GRr=GenomicRanges::resize(bed.GR, width=receptive_field, fix="center")

region.seq=as.character(
  BSgenome::getSeq(
    BSgenome.Sscrofa.UCSC.susScr11::BSgenome.Sscrofa.UCSC.susScr11,
    names=GenomicRanges::seqnames(region.GRr),
    start=GenomicRanges::start(region.GRr),
    end=GenomicRanges::end(region.GRr)
  )
)

# replace LW/MS alleles in sequences
SNPposrel=ceiling((receptive_field+1)/2)

region.seqLW=region.seq
substring(region.seqLW,SNPposrel,SNPposrel)=as.character(region.GRr$LW)
region.seqLW=Biostrings::DNAStringSet(region.seqLW)
names(region.seqLW)=paste0("LW",1:length(region.seqLW))

region.seqMS=region.seq
substring(region.seqMS,SNPposrel,SNPposrel)=as.character(region.GRr$MS)
region.seqMS=Biostrings::DNAStringSet(region.seqMS)
names(region.seqMS)=paste0("MS",1:length(region.seqMS))

Biostrings::writeXStringSet(x=region.seqLW,filepath="data/enformer_impact_scores/individual_mutations/20240307_VariantsPhases_ok.LWsnp.fa")
Biostrings::writeXStringSet(x=region.seqMS,filepath="data/enformer_impact_scores/individual_mutations/20240307_VariantsPhases_ok.MSsnp.fa")
bed.GR <- GenomicRanges::GRanges(unlist(GenomicRanges::tile(GenomicRanges::resize(bed.GR, width=window, fix="center"), width=bin.size)))
write.table(as.data.frame(bed.GR)[,-c(4,5)],file="results/enformer/240307_impact_scores_Lw_MS/20240307_VariantsPhases_ok.snp.bed",
            col.names=T,row.names=F,sep='\t',quote=F)

# clean objects from the workspace
rm(list=ls())
gc()

# manage INDELS
# read indels table and separate it into insertions and deletions tables
dataINDELS <- read.table("data/deepbind_impact_scores/20231005_indels.csv", sep=";", header=T)  # substring does not mutate INDELs
dataINS <- data.table::rbindlist(lapply(
  seq_len(nrow(dataINDELS)),
  function(i){
    if (nchar(dataINDELS$REF[i])<nchar(dataINDELS$ALT[i])){
      return(dataINDELS[i,])
    }
  }))
dataDELS <- data.table::rbindlist(lapply(
  seq_len(nrow(dataINDELS)),
  function(i){
    if (nchar(dataINDELS$REF[i])>nchar(dataINDELS$ALT[i])){
      return(dataINDELS[i,])
    }
  }))
rm(dataINDELS)
gc()

dataINS$CHR <- "chr1"
data.table::setnames(dataINS, c("POS", "LW", "MS"), c("START", "LWhap", "MShap"))
dataINS$END <- unlist(lapply(
  seq_len(nrow(dataINS)),
  function(i){
    dataINS$START[i]+max(nchar(dataINS$REF[i]), nchar(dataINS$ALT[i]))
  }
))
data.table::setcolorder(dataINS, c('CHR', 'START', 'END', 'REF', 'ALT', 'LWhap', 'MShap'))

# replace 0s and 1s to REF and ALT content
dataINS$LWall <- unlist(lapply(
  seq_len(nrow(dataINS)),
  function(x){
    if(dataINS$LWhap[x]==0){
      dataINS$REF[x]
    }else{
      dataINS$ALT[x]
    }
  }
))
dataINS$MSall <- unlist(lapply(
  seq_len(nrow(dataINS)),
  function(x){
    if(dataINS$MShap[x]==0){
      dataINS$REF[x]
    }else{
      dataINS$ALT[x]
    }
  }
))

dataINS <- data.frame(dataINS)

receptive_field=393216  # receptive field of enformer
window=114688  # predicted window of enformer is 114688
bin.size=128

# setup GRanges with LW and MS alleles and get sequences
bed.GR=GenomicRanges::GRanges(
  dataINS[,1],
  IRanges::IRanges(dataINS[,2], dataINS[,3]),
  LW=dataINS[,8],
  MS=dataINS[,9]
)
region.GRr=GenomicRanges::resize(bed.GR, width=receptive_field, fix="center")

region.seq=as.character(
  BSgenome::getSeq(
    BSgenome.Sscrofa.UCSC.susScr11::BSgenome.Sscrofa.UCSC.susScr11,
    names=GenomicRanges::seqnames(region.GRr),
    start=GenomicRanges::start(region.GRr),
    end=GenomicRanges::end(region.GRr)
  )
)

INSposrel <- floor((receptive_field+1-GenomicRanges::width(bed.GR))/2)+1

region.seqLW <- unlist(lapply(
  seq_along(region.seq),
  function(i){
    x=region.seq[i]
    pos=INSposrel[i]
    ins=dataINS$LWall[i]
    # paste0(before pos, insertion, after pos)
    y=paste0(substr(x, 1, pos-1), ins, substr(x, pos+1, nchar(x)))
    return(substr(y, 1, receptive_field))
  }
))
region.seqLW=Biostrings::DNAStringSet(region.seqLW)
names(region.seqLW)=paste0("LW",1:length(region.seqLW))

region.seqMS <- unlist(lapply(
  seq_along(region.seq),
  function(i){
    x=region.seq[i]
    pos=INSposrel[i]
    ins=dataINS$MSall[i]
    # paste0(before pos, insertion, after pos)
    y=paste0(substr(x, 1, pos-1), ins, substr(x, pos+1, nchar(x)))
    return(substr(y, 1, receptive_field))
  }
))
region.seqMS=Biostrings::DNAStringSet(region.seqMS)
names(region.seqMS)=paste0("MS",1:length(region.seqMS))

Biostrings::writeXStringSet(x=region.seqLW,filepath="data/enformer_impact_scores/individual_mutations/20240306_VariantsPhases_ok.LWins.fa")
Biostrings::writeXStringSet(x=region.seqMS,filepath="data/enformer_impact_scores/individual_mutations/20240306_VariantsPhases_ok.MSins.fa")
bed.GR <- GenomicRanges::GRanges(unlist(GenomicRanges::tile(GenomicRanges::resize(bed.GR, width=window, fix="center"), width=bin.size)))
write.table(as.data.frame(bed.GR)[,-c(4,5)],file="results/enformer/240307_impact_scores_Lw_MS/20240306_VariantsPhases_ok.ins.bed",
            col.names=T,row.names=F,sep='\t',quote=F)


# clean objects except dels table from the workspace
rm(list=setdiff(ls(), "dataDELS"))
gc()


dataDELS$CHR <- "chr1"
data.table::setnames(dataDELS, c("LW", "MS"), c("LWhap", "MShap"))
data.table::setcolorder(dataDELS, c('CHR', 'POS', 'REF', 'ALT', 'LWhap', 'MShap'))

# replace 0s and 1s to REF and ALT content
dataDELS$LWall <- unlist(lapply(
  seq_len(nrow(dataDELS)),
  function(x){
    if(dataDELS$LWhap[x]==0){
      dataDELS$REF[x]
    }else{
      dataDELS$ALT[x]
    }
  }
))
dataDELS$MSall <- unlist(lapply(
  seq_len(nrow(dataDELS)),
  function(x){
    if(dataDELS$MShap[x]==0){
      dataDELS$REF[x]
    }else{
      dataDELS$ALT[x]
    }
  }
))

dataDELS <- data.frame(dataDELS)

receptive_field=393216  # receptive field of enformer
window=114688  # predicted window of enformer is 114688
bin.size=128

# setup GRanges with LW and MS alleles and get sequences
bed.GR=GenomicRanges::GRanges(
  dataDELS[,1],
  IRanges::IRanges(dataDELS[,2], dataDELS[,2]),
  LW=dataDELS[,7],
  MS=dataDELS[,8]
)
widths <- vapply(
  seq_along(bed.GR),
  function(i){max(nchar(bed.GR$LW[i]), nchar(bed.GR$MS)[i])},
  FUN.VALUE=numeric(1L)
)+receptive_field
region.GRr=GenomicRanges::resize(bed.GR, width=widths, fix="center")

region.seq=as.character(
  BSgenome::getSeq(
    BSgenome.Sscrofa.UCSC.susScr11::BSgenome.Sscrofa.UCSC.susScr11,
    names=GenomicRanges::seqnames(region.GRr),
    start=GenomicRanges::start(region.GRr),
    end=GenomicRanges::end(region.GRr)
  )
)

# replace LW/MS alleles in sequences
DELSposrel=ceiling((widths+1)/2)

region.seqLW <- unlist(lapply(
  seq_along(region.seq),
  function(i){
    x=region.seq[i]
    pos=DELSposrel[i]
    hap=dataDELS$LWall[i]
    # paste0(before pos, insertion, after pos)
    if (nchar(hap)==1){
      # if deletion
      y=paste0(substr(x, 1, pos-1), hap, substr(x, pos+nchar(dataDELS$REF[i]), nchar(x)-1))
    }else{
      # if no deletion
      y=paste0(substr(x, 1, pos-1), hap, substr(x, pos+nchar(hap), receptive_field))
    }
    return(y)
  }
))
region.seqLW=Biostrings::DNAStringSet(region.seqLW)
names(region.seqLW)=paste0("LW",1:length(region.seqLW))

region.seqMS <- unlist(lapply(
  seq_along(region.seq),
  function(i){
    x=region.seq[i]
    pos=DELSposrel[i]
    hap=dataDELS$MSall[i]
    # paste0(before pos, insertion, after pos)
    if (nchar(hap)==1){
      # if deletion
      y=paste0(substr(x, 1, pos-1), hap, substr(x, pos+nchar(dataDELS$REF[i]), nchar(x)-1))
    }else{
      # if no deletion
      y=paste0(substr(x, 1, pos-1), hap, substr(x, pos+nchar(hap), receptive_field))
    }
    
    return(y)
  }
))
region.seqMS=Biostrings::DNAStringSet(region.seqMS)
names(region.seqMS)=paste0("MS",1:length(region.seqMS))

Biostrings::writeXStringSet(x=region.seqLW,filepath="data/enformer_impact_scores/individual_mutations/20240306_VariantsPhases_ok.LWdels.fa")
Biostrings::writeXStringSet(x=region.seqMS,filepath="data/enformer_impact_scores/individual_mutations/20240306_VariantsPhases_ok.MSdels.fa")
bed.GR <- GenomicRanges::GRanges(unlist(GenomicRanges::tile(GenomicRanges::resize(bed.GR, width=window, fix="center"), width=bin.size)))
write.table(as.data.frame(bed.GR)[,-c(4,5)],file="results/enformer/240307_impact_scores_Lw_MS/20240306_VariantsPhases_ok.dels.bed",
            col.names=T,row.names=F,sep='\t',quote=F)
