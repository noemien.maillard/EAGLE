#!/usr/bin/bash


# species dna
output=/media/nomaillard/Storage/EAGLE/results/deepbind/auc_deepbind/Hsapiens
pred_path=/media/nomaillard/Storage/EAGLE/results/deepbind/Hsapiens/02_bigwig

# proteins and observations
proteins=(MAX MYC NANOG YY1 SIN3A RBBP5 POU5F1 SUZ12 CTCF)
obs_path=/media/nomaillard/Storage/EAGLE/data/Hsapiens/deepbind_training/02_liftover

for protein in ${proteins[@]}
do
	./01_auROC_auPRC_parser.R \
		--pred_path $pred_path \
		--protein $protein \
		--obs_path $obs_path \
		--output $output/$protein.tsv \
		--lifted \
		--log $output/$protein.log
done
