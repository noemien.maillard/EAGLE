#!/usr/bin/bash


# species dna
output=/media/nomaillard/Storage/EAGLE/results/deepbind/auc_deepbind/02_panel2/Ggallus
bins=$(ls /media/nomaillard/Storage/EAGLE/data/deepbind_genome_binned/deepbind_Gallus_gallus_genome_binned/*/*.bed)
pred_path=/media/nomaillard/Storage/EAGLE/results/deepbind/Ggallus/02_bigwig

# proteins and observations
proteins=(CTCF)
obs_path=/media/nomaillard/Storage/EAGLE/data/Ggallus/bed_ncbi_geo

for protein in ${proteins[@]}
do
	./01_auROC_auPRC_parser.R \
		--pred_path $pred_path \
		--protein $protein \
		--obs_path $obs_path \
		--output $output/$protein.tsv \
		--fread_obs \
		--add_chr \
		--log $output/$protein.log
done
